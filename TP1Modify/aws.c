#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#define TAM 256

//Definiciones utilizadas al enviar el archivo
#define BUFFSIZE 1
#define	ERROR	-1

char port[] = "25";
char cod1[] ="get_datta";
char cod2[] ="get_telemetry";
char cod3[] ="erase_datta";
char cod4[] ="disconnect";

char nameFile[] = "datta.txt";

int quantityLines()
{
     FILE *entrada;
   int ch, num_lineas;

   if ((entrada = fopen(nameFile, "r")) == NULL){
      perror(nameFile);
      return EXIT_FAILURE;
   }

   num_lineas = 0;
   while ((ch = fgetc(entrada)) != EOF)
      if (ch == '\n')
         num_lineas++;
    return num_lineas;
}

int equals(char *cod,char *buffer){

    int c;
    for(c=0;c<strlen(cod);c+=1){
        if(buffer[c]!=cod[c])
        {
            return 0;
        }
    }
    return 1;
}


char * readFile(char *file,int number)
{
    char * content = malloc(400 * sizeof(char));
    FILE *fp;
    int c = number + 1;
    fp = fopen(file, "r");
     if (fp == NULL)
        exit(EXIT_FAILURE);

    int cont;
    for(cont=0;cont<=c;cont+=1)
    {
        fgets(content, 400, fp);
    }

    fclose(fp);

    return content;
}

char * lastLine(){
    int c ;
    int total = quantityLines()-1;
    char *lin;
    for(c=0;c<total;c+=1)
    {
       lin = readFile(nameFile,c);
    }

    return lin;
}


void deleteFile()
{
    char lin[] = "";
        FILE *fp;

        fp = fopen ( "datta.txt", "w" );

        fprintf(fp, lin);

        fclose ( fp );
}

int checkPort(char * port){

	int port2 = atoi(port);
	if(port2>=20 && port2 <=44 )
		return 1;
	else
		return 0;

}


int main()
{
  int sockfd, puerto , pid;
  char buffer[TAM];
  struct sockaddr_in serv_addr, cli_addr;
  int n;

  socklen_t clilen;

  ssize_t newsockfd;

  sockfd = socket( AF_INET, SOCK_STREAM, 0);
  if ( sockfd < 0 ) {
    perror( " apertura de socket ");
    exit( 1 );
  }

  memset( (char *) &serv_addr, 0, sizeof(serv_addr) );

  if(!checkPort(port)){
  	printf("Error, verifique el numero de la estacion\n");
  	exit(0);
  }

  char puer[4];
  puer[0]='6';
  puer[1]='0';
  puer[2]=port[0];
  puer[3]=port[1];
  puerto = atoi( puer );
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons( puerto );

  if ( bind(sockfd, ( struct sockaddr *) &serv_addr, sizeof( serv_addr ) ) < 0 ) {
    perror( "ligadura" );
    exit( 1 );
  }

        printf( "Proceso: %d - socket disponible: %d\n", getpid(), ntohs(serv_addr.sin_port) );

  listen( sockfd, 5 );
  clilen = sizeof( cli_addr );

  while( 1 ) {
    newsockfd = accept( sockfd, (struct sockaddr *) &cli_addr, &clilen );
    if ( newsockfd < 0 ) {
      perror( "accept" );
      exit( 1 );
    }

    pid = fork();
    if ( pid < 0 ) {
      perror( "fork" );
      exit( 1 );
    }

    if ( pid == 0 ) {  // Proceso hijo
      close( sockfd );

      while ( 1 ) {
        memset( buffer, 0, TAM );

        n = read( newsockfd, buffer, TAM-1 );
        if ( n < 0 ) {
          perror( "lectura de socket" );
          exit(1);
        }

                //leer parámetros

                if(equals( cod2 , buffer )){

                    int total=quantityLines();

                    if(total == 0)
                    {
                        n = write( newsockfd, "No data today", 255 );
                    }else{
                        char *last = lastLine();
                         n = write( newsockfd, last, 255 );
                    }

                    if ( n < 0 ) {
                        perror( "escritura en socket" );
                        exit( 1 );
                    }
                } else if(equals( cod1 , buffer )){
                    //Debo crear socket UDP
                        int sockfd2, puerto2 ;
                        char buffer2[ TAM ];
                        struct sockaddr_in serv_addr2;
                        ssize_t n2;

                        socklen_t tamano_direccion2;


                        sockfd2 = socket( AF_INET, SOCK_DGRAM, 0 );
                        if (sockfd2 < 0) {
                            perror("ERROR en apertura de socket");
                            exit( 1 );
                        }

                        memset( &serv_addr2, 0, sizeof(serv_addr2) );
                        puerto2 = atoi( puer );
                        serv_addr2.sin_family = AF_INET;
                        serv_addr2.sin_addr.s_addr = INADDR_ANY;
                        serv_addr2.sin_port = htons( puerto2 );
                        memset( &(serv_addr2.sin_zero), '\0', 8 );

                        if( bind( sockfd2, (struct sockaddr *) &serv_addr2, sizeof(serv_addr2) ) < 0 ) {
                            perror( "ERROR en binding" );
                            exit( 1 );
                        }

                            printf( "Socket UDP disponible: %d\n", ntohs(serv_addr2.sin_port) );

                        tamano_direccion2 = sizeof( struct sockaddr );

                            memset( buffer2, 0, TAM );
                            n2 = recvfrom( sockfd2, buffer2, TAM-1, 0, (struct sockaddr *)&serv_addr2, &tamano_direccion2 );
                            if ( n2 < 0 ) {
                                perror( "lectura de socket UDP" );
                                exit( 1 );
                            }
                            printf( "Command UDP: %s\n", buffer2 );

                                FILE *archivo;
                                archivo = fopen("datta.txt","rb");
                                if(!archivo){
                                    perror("Error al abrir el archivo:");
                                    exit(EXIT_FAILURE);
                                }

                                char buffer3[BUFFSIZE];

                                while(!feof(archivo)){
                                    fread(buffer3,sizeof(char),BUFFSIZE,archivo);
                                    //if(send(sockfd2,buffer3,BUFFSIZE,0) == ERROR)
                                    if(sendto( sockfd2, buffer3, sizeof(char), 0, (struct sockaddr *)&serv_addr2, tamano_direccion2  ) == ERROR)

                                        perror("Error al enviar el arvhivo:");
                                }

                            char message1[] ="#";
                           sendto( sockfd2, message1, sizeof(char) * 2048, 0, (struct sockaddr *)&serv_addr2, tamano_direccion2  );

                             close( sockfd2 );
                            printf( "Connection UDP finished\n ");

                    n = write( newsockfd, "Connection UDP finished\n", 255 );

                    if ( n < 0 ) {
                        perror( "escritura en socket" );
                        exit( 1 );
                    }
                } else if(equals( cod3 , buffer )){
                    deleteFile();
                    n = write( newsockfd, "Deleted all data", 255 );
                    if ( n < 0 ) {
                        perror( "escritura en socket" );
                        exit( 1 );
                    }
                } else if(equals( cod4 , buffer )){

                    n = write( newsockfd, "Connection finished", 255 );
                    if ( n < 0 ) {
                        perror( "escritura en socket" );
                        exit( 1 );
                    }

                    printf( "Connection finished \n\n");
                    exit(0);

                } else{
                     n = write( newsockfd, "Error: command not recognized ", 255 );
                    if ( n < 0 ) {
                        perror( "escritura en socket" );
                        exit( 1 );
                    }
                }

      }
    }
    else {
      printf( "Nuevo cliente!!\n" );
      close( newsockfd );
    }
  }
	return 0;
}
