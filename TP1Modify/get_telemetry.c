#include <stdio.h>
#include <stdlib.h>

char nameFile[] = "datta.txt";

int quantityLines()
{
     FILE *entrada;
   int ch, num_lineas;

   if ((entrada = fopen(nameFile, "r")) == NULL){
      perror(nameFile);
      return EXIT_FAILURE;
   }

   num_lineas = 0;
   while ((ch = fgetc(entrada)) != EOF)
      if (ch == '\n')
         num_lineas++;
    return num_lineas;
}

char * readFile(char *file,int number)
{
    char * content = malloc(400 * sizeof(char));
    FILE *fp;
    int c = number + 1;
    fp = fopen(file, "r");
     if (fp == NULL)
        exit(EXIT_FAILURE);

    int cont;
    for(cont=0;cont<=c;cont+=1)
    {
        fgets(content, 400, fp);
    }

    fclose(fp);

    return content;
}


char * lastLine(){
    int c ;
    int total = quantityLines()-1;
    char *lin;
    for(c=0;c<total;c+=1)
    {
       lin = readFile(nameFile,c);
    }

    return lin;
}


char * getForm(char * buffer,int num){
    char * text= malloc(17 * sizeof(char));;

    int total=0;
    int terminado = 0;
    int x=0;

    while(terminado == 0){
     char letter = (char)(buffer[x]);
       if( letter == ','){
            total+=1;
       }
       if(letter == '\0')
       {
            terminado=1;
            break;
       }
       int c=0;
       if(total == num){
           terminado=1;
           int k=0;
           if(x>0){x+=1;}
           for(;k<=25;k+=1){
                letter =  (char)(buffer[x]);
                x+=1;
                if(letter=='\0' || letter=='\n'|| letter==','){
                    text[c]='\0';
                    break;
                }else{
                    text[c]=letter;
                    c+=1;
                }
           }
       }
       x+=1;
    }
    return text;
}


int main(int argc, char const *argv[])
{
	int total=quantityLines();
	printf("Content-type: text/html\n\n");
    printf("<html>\n");
    printf("<head><title>Get_Telemetry</title></head>\n");
    printf("<body><h2>Get_Telemetry</h2><pre>");

    if(total == 0)
    {
        printf("No data today");
    }else{

        char *last = lastLine();
        printf("%s\n", last);

        printf("Tiempo: %s \n", getForm(last,0) );
        printf("Precipitación: %s \n", getForm(last,1) );
        printf("Humedad Relativa: %s \n", getForm(last,2) );
        printf("Temperatura 1.5m: %s \n", getForm(last,3) );
        printf("Temperatura a 10cm: %s \n", getForm(last,4) );
        printf("\n");
    }
	return 0;
}