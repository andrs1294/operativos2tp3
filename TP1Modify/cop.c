#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#define TAM 256

/*Definición para descarga archivo*/
#define BUFFSIZE 1
#define ERROR		-1


char nameAWS[2];


int checkArgumentConnect(char *command)
{
    char co[]="connect";
    int c;
    for(c=0;c<7;c+=1){
        if( co[c] !=  ((char)(command[c])) ){
            return 0;
        }
    }

    return 1;
}

int check3Argument(char *command)
{
    int total=0;
    int terminado = 0;
    int x=0;

    while(terminado == 0){
     char letter = (char)(command[x]);
       if( letter == ' '){
            total+=1;
       }
       if(letter == '\0')
       {
            terminado=1;
       }
       if(total == 3){
           terminado=1;
       }
       x+=1;
    }

    if(total >=2){
        return 1;
    }else{
        return 0;
    }

}

char * getPort( char *command){
    char * port = malloc(6 * sizeof(char));
    //char port[6];
    int total=0;
    int terminado = 0;
    int x=0;

    while(terminado == 0){
     char letter = (char)(command[x]);
       if( letter == ' '){
            total+=1;
       }
       if(letter == '\0')
       {
            terminado=1;
       }
       int c=0;
       if(total == 2){
           terminado=1;
           int k=0;
           x+=1;
           for(;k<=5;k+=1){
                letter =  (char)(command[x]);
                x+=1;
                if(letter==' ' || letter=='\0' || letter=='\n'){
                    port[c]='\0';
                    break;
                }else{
                    port[c]=letter;
                    c+=1;
                }
           }
       }
       x+=1;
    }
    return port;
}

char * getHost( char *command){
    char * host = malloc(17 * sizeof(char));
 //   char host[17];
    int total=0;
    int terminado = 0;
    int x=0;

    while(terminado == 0){
     char letter = (char)(command[x]);
       if( letter == ' '){
            total+=1;
       }
       if(letter == '\0')
       {
            terminado=1;
       }
       int c=0;
       if(total == 1){
           terminado=1;
           int k=0;
           x+=1;
           for(;k<=16;k+=1){
                letter =  (char)(command[x]);
                x+=1;
                if(letter==' ' || letter=='\0' || letter=='\n'){
                    host[c]='\0';
                    break;
                }else{
                    host[c]=letter;
                    c+=1;
                }
           }
       }
       x+=1;
    }
    return host;
}

int atoi_skip(char *s) {
        while(*s != '\0' && !isdigit(*s)) s++; /* skip non digits */
        return atoi(s);
}

void getAWS(char * port){
    int c=2;
    char first=port[0];
    char second=port[1];
    char letter=port[2];
    while(letter != '\0' && letter != ' ' && letter != '\n')
    {
        first=port[c-1];
        second=port[c];
        c+=1;
        letter=port[c];
    }
    nameAWS[0]=first;
    nameAWS[1]=second;
}


char * getForm(char * buffer,int num){
    char * text= malloc(17 * sizeof(char));;

    int total=0;
    int terminado = 0;
    int x=0;

    while(terminado == 0){
     char letter = (char)(buffer[x]);
       if( letter == ','){
            total+=1;
       }
       if(letter == '\0')
       {
            terminado=1;
            break;
       }
       int c=0;
       if(total == num){
           terminado=1;
           int k=0;
           if(x>0){x+=1;}
           for(;k<=25;k+=1){
                letter =  (char)(buffer[x]);
                x+=1;
                if(letter=='\0' || letter=='\n'|| letter==','){
                    text[c]='\0';
                    break;
                }else{
                    text[c]=letter;
                    c+=1;
                }
           }
       }
       x+=1;
    }
    return text;
}

int main()
{
    char *hostN;
    printf("Welcome!\n");
    printf("Commands: connect <ip> <port> | disconnect | get_datta | get_telemetry | erase_datta \n\n");
    while(1){
        int sockfd, puerto, n;
        struct sockaddr_in serv_addr;
        struct hostent *server;
        int terminar = 0;
        char buffer[TAM];



        printf("Admin@COP:-$");

        char command[255];
        fgets(command,255,stdin);

        if( checkArgumentConnect(command) )
        {
             if( check3Argument(command) ){

                char *port=getPort(command);
                char *host=getHost(command);
                hostN = host;
                puerto = atoi_skip( port );
                getAWS(port);
                sockfd = socket( AF_INET, SOCK_STREAM, 0 );
                if ( sockfd < 0 ) {
                    perror( "ERROR apertura de socket" );
                    exit( 1 );
                }

                server = gethostbyname( host );
                if (server == NULL) {
                    fprintf( stderr,"Error, no existe el host\n" );
                    exit( 0 );
                }

                memset( (char *) &serv_addr, '0', sizeof(serv_addr) );
                serv_addr.sin_family = AF_INET;
                bcopy( (char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length );
                serv_addr.sin_port = htons( puerto );

                if ( connect( sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr ) ) < 0 ) {
                    perror( "conexion" );
                    exit( 1 );
                }else{
                     printf( "Conexión exitosa\n" );
                }

            char prompt[80];
            strcpy(prompt, "AWS");
            strcat(prompt, nameAWS);
            strcat(prompt, "@COP:-$");

            while(1) {

                printf( prompt );
                memset( buffer, '\0', TAM );
                fgets( buffer, TAM-1, stdin );

                n = write( sockfd, buffer, strlen(buffer) );
                if ( n < 0 ) {
                    perror( "escritura de socket" );
                    exit( 1 );
                }

                buffer[strlen(buffer)-1] = '\0';
                if( !strcmp( "get_datta", buffer ) ) {

                    printf( "Realizando conexión UDP\n" );
                    sleep(1);

                    int sockfd2, puerto2, n2, tamano_direccion2;
                    struct sockaddr_in dest_addr2;
                    struct hostent *server2;

                    char buffer2[TAM];

                    server2 = gethostbyname( host );
                    if ( server2 == NULL ) {
                        fprintf( stderr, "ERROR, no existe el host\n");
                        exit(0);
                    }

                    puerto2 = atoi( port );
                    sockfd2 = socket( AF_INET, SOCK_DGRAM, 0 );
                    if (sockfd2 < 0) {
                        perror( "apertura de socket" );
                        exit( 1 );
                    }

                    dest_addr2.sin_family = AF_INET;
                    dest_addr2.sin_port = htons( atoi( port ) );
                    dest_addr2.sin_addr = *( (struct in_addr *)server2->h_addr );
                    memset( &(dest_addr2.sin_zero), '\0', 8 );
                    memset( buffer2, 0, TAM );

                    tamano_direccion2 = sizeof( dest_addr2 );
                    n2 = sendto( sockfd2, "get_datta", TAM, 0, (struct sockaddr *)&dest_addr2, tamano_direccion2 );
                    if ( n2 < 0 ) {
                        perror( "Escritura en socket" );
                        exit( 1 );
                    }
                    memset( buffer2, 0, sizeof( buffer2 ) );


                 printf( "UDP: Download started\n" );
                        char buffer3[BUFFSIZE];
                        memset( buffer3, 0, sizeof( buffer3 ) );
                        int recibido = -1;

                        /*Se abre el archivo para escritura*/
                        FILE *file2 = fopen("archivoRecibido.txt","wb");

                        while((recibido = recv(sockfd2, buffer3, BUFFSIZE, 0)) > 0){


                            if(buffer3[0]=='#'){
                                break;
                            }
                            fwrite(buffer3,sizeof(char),1,file2);
                        }//Termina la recepción del archivo

                        fclose(file2);

                    printf( "UDP: Download Completed\n" );

                }

                if( !strcmp( "disconnect", buffer ) ) {
                    terminar = 1;
                }

                if(!strcmp( "exit", buffer )){
                    exit(0);
                }
                int setFormat = 0;
                if(!strcmp( "get_telemetry", buffer )){
                    setFormat=1;
                }

                memset( buffer, '\0', TAM );
                n = read( sockfd, buffer, TAM );
                if ( n < 0 ) {
                    perror( "lectura de socket" );
                    exit( 1 );
                }

                printf( ": %s\n", buffer );
                if(setFormat)
                {
                    printf("Tiempo: %s \n", getForm(buffer,0) );
                    printf("Precipitación: %s \n", getForm(buffer,1) );
                    printf("Humedad Relativa: %s \n", getForm(buffer,2) );
                    printf("Temperatura 1.5m: %s \n", getForm(buffer,3) );
                    printf("Temperatura a 10cm: %s \n", getForm(buffer,4) );
                    printf("\n");
                    setFormat=0;
                }
                if( terminar ) {
                    break;
                }
              }

             }else{
                printf("command not recognized. \n");
             }
        }else{
            if(strcmp(command,"exit\n")==0)
            {
                exit(0);
            }else{
                printf(":command not recognized or not conection. \n");

            }
        }
    }

    return 0;

}
