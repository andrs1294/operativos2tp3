all:
	gcc getInfo/getInfoCPU.c -o html/cgi-bin/getInfo.cgi
	gcc getInfo/getInfoMemo.c -o html/cgi-bin/getInfoMemo.cgi
	gcc getInfo/getInfoTime.c -o html/cgi-bin/getInfoTime.cgi
	gcc getModules/getModules.c -o html/cgi-bin/getModules.cgi
	gcc Modulo/install.c -o html/cgi-bin/install.cgi
	gcc Modulo/uninstall.c -o html/cgi-bin/uninstall.cgi
	gcc Modulo/upload.c -o html/cgi-bin/upload.cgi
	gcc TP1Modify/erase_datta.c -o html/cgi-bin/erase_datta.cgi
	gcc TP1Modify/get_datta.c -o html/cgi-bin/get_datta.cgi
	gcc TP1Modify/get_telemetry.c -o html/cgi-bin/get_telemetry.cgi