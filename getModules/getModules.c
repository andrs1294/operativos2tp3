#include <stdio.h>
#include <stdlib.h>

char * file = "modules.txt";
void writeFile(char * msg)
{    
    FILE *fp = fopen(file, "a");
    if(fp == NULL)
    {
        printf("Error al abrir archivo");
    }
    fprintf(fp, msg);
    fprintf(fp, "\n");
    fclose(fp);      
}

void readFile()
{
    
    FILE *fp;
    char c;
    fp = fopen(file, "r");
     if (fp == NULL)
        exit(EXIT_FAILURE);

   while (!feof (fp)) {
      c = getc (fp);
      printf ("%c", c);
   }


    fclose(fp);

}

int quantityLines()
{
     FILE *entrada;
   int ch, num_lineas;

   if ((entrada = fopen(file, "r")) == NULL){
      perror(file);
      return EXIT_FAILURE;
   }

   num_lineas = 0;
   while ((ch = fgetc(entrada)) != EOF)
      if (ch == '\n')
         num_lineas++;
    return num_lineas;
}

void deleteFile()
{
    char lin[] = "";
        FILE *fp;

        fp = fopen ( file, "w" );

        fprintf(fp, lin);

        fclose ( fp );
}

void writeMemoInfo()
{
	
    writeFile("<pre>");
    system("lsmod >> modules.txt");
    writeFile("</pre>");

}



int main(int argc, char const *argv[])
{
	deleteFile();
	writeFile("Content-type: text/html\n\n") ;
	writeFile("<html>\n") ;
    writeFile("<head><title>Modules System</title></head>\n") ;
    writeFile("<body>\n") ;
    writeFile("<hr>\n") ;
    writeMemoInfo();
    writeFile("<hr>\n") ;
    writeFile("</body>\n") ;
    writeFile("</html>\n") ;
    

	int c ;
    int total = quantityLines();
    if(total==0)
    {
    	printf("Vacio");
    }else{
	       readFile();
	       
    }

    
	return 0;
}