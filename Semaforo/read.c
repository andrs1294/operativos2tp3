#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdlib.h>

//
// Esta union hay que definirla o no según el valor de los defines aqui
// indicados.
//        
#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
// La union ya está definida en sys/sem.h
#else
// Tenemos que definir la union
union semun 
{ 
  int val;
  struct semid_ds *buf;
  unsigned short int *array;
  struct seminfo *__buf;
};
#endif

char nameFile[] = "delta.csv";

char * readFile(char *file,int number)
{
    char * content = malloc(400 * sizeof(char));
    FILE *fp;
    int c = number + 1;
    fp = fopen(file, "r");
     if (fp == NULL)
        exit(EXIT_FAILURE);

    int cont;
    for(cont=0;cont<=c;cont+=1)
    {
        fgets(content, 400, fp);
    }

    fclose(fp);

    return content;
}

int quantityLines()
{
     FILE *entrada;
   int ch, num_lineas;

   if ((entrada = fopen(nameFile, "r")) == NULL){
      perror(nameFile);
      return EXIT_FAILURE;
   }

   num_lineas = 0;
   while ((ch = fgetc(entrada)) != EOF)
      if (ch == '\n')
         num_lineas++;
    return num_lineas;
}

void deleteFile()
{
    char lin[] = "";
        FILE *fp;

        fp = fopen ( "datta.txt", "w" );

        fprintf(fp, lin);

        fclose ( fp );
}

int main()
{

    deleteFile();
    int c ;
    int total = quantityLines()-1;

   key_t Clave;
   int Id_Semaforo;
   struct sembuf esperar;
   struct sembuf liberar;
   union semun arg;
   int i=0;

    Clave = ftok ("datta.txt", 86);
    if (Clave == (key_t)-1)
    {
      printf("No puedo conseguir clave de semáforo\n");
      exit(0);
    }

    Id_Semaforo = semget (Clave, 10, 0600 | IPC_CREAT);
    if (Id_Semaforo == -1)
    {
       printf("No puedo crear semáforo\n");
      exit (0);
    }

    arg.val = 1;
    semctl (Id_Semaforo, 0, SETVAL, 1);
    esperar.sem_num = 0;
    esperar.sem_op = -1;
    esperar.sem_flg = 0;

    liberar.sem_num = 0;
    liberar.sem_op = 1;
    liberar.sem_flg = 0;

    for(c=0;c<total;c+=1)
    {
        
        char *lin = readFile(nameFile,c);
        FILE *fp;

        semop (Id_Semaforo, &esperar, 1);
        fp = fopen ( "datta.txt", "a" );

        fprintf(fp, lin);

        fclose ( fp );

        semop (Id_Semaforo, &liberar, 1);

        sleep(1);
        //printf("%s\n", lin );

    }

    return 0;
}
