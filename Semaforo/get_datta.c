#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFFSIZE 1

#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
// La union ya está definida en sys/sem.h
#else
// Tenemos que definir la union
union semun 
{ 
  int val;
  struct semid_ds *buf;
  unsigned short int *array;
  struct seminfo *__buf;
};
#endif


int main(int argc, char const *argv[])
{


    key_t Clave;
    int Id_Semaforo;
    struct sembuf esperar;
    struct sembuf liberar;
    union semun arg;

    Clave = ftok ("datta.txt", 86);
    if (Clave == (key_t)-1)
    {
        printf("No puedo conseguir clave de semáforo");
        exit(0);
    }

    Id_Semaforo = semget (Clave, 10, 0600 | IPC_CREAT);
    if (Id_Semaforo == -1)
    {
        printf("No puedo crear semáforo");
        exit (0);
    }

    esperar.sem_num = 0;
    esperar.sem_op = -1;
    esperar.sem_flg = 0;

    liberar.sem_num = 0;
    liberar.sem_op = 1;
    liberar.sem_flg = 0;

    semop (Id_Semaforo, &esperar, 1);


    FILE *archivo;
    archivo = fopen("datta.txt","rb");
    if(!archivo){
        perror("Error al abrir el archivo:");
        exit(EXIT_FAILURE);
    }

    char buffer3[BUFFSIZE];

    printf("Content-type: text/html\n\n");
    printf("<html>\n");
    printf("<head><title>Get_datta</title></head>\n");
    printf("<body><h2>Get_Datta</h2><pre>");
    while(!feof(archivo)){
        fread(buffer3,sizeof(char),BUFFSIZE,archivo);
        printf("%s",buffer3 );
            
    }

    fclose ( archivo );

    printf("Liberar Semáforo\n");
        semop (Id_Semaforo, &liberar, 1);

    return 0;
}


