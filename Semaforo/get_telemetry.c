#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <unistd.h>

#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
// La union ya está definida en sys/sem.h
#else
// Tenemos que definir la union
union semun 
{ 
  int val;
  struct semid_ds *buf;
  unsigned short int *array;
  struct seminfo *__buf;
};
#endif



char nameFile[] = "datta.txt";

int quantityLines()
{
     FILE *entrada;
   int ch, num_lineas;

   if ((entrada = fopen(nameFile, "r")) == NULL){
      perror(nameFile);
      return EXIT_FAILURE;
   }

   num_lineas = 0;
   while ((ch = fgetc(entrada)) != EOF)
      if (ch == '\n')
         num_lineas++;
    return num_lineas;
}

char * readFile(char *file,int number)
{
    char * content = malloc(400 * sizeof(char));
    FILE *fp;
    int c = number + 1;
    fp = fopen(file, "r");
     if (fp == NULL)
        exit(EXIT_FAILURE);

    int cont;
    for(cont=0;cont<=c;cont+=1)
    {
        fgets(content, 400, fp);
    }

    fclose(fp);

    return content;
}


char * lastLine(){
    int c ;
    int total = quantityLines()-1;
    char *lin;
    for(c=0;c<total;c+=1)
    {
       lin = readFile(nameFile,c);
    }

    return lin;
}


char * getForm(char * buffer,int num){
    char * text= malloc(17 * sizeof(char));;

    int total=0;
    int terminado = 0;
    int x=0;

    while(terminado == 0){
     char letter = (char)(buffer[x]);
       if( letter == ','){
            total+=1;
       }
       if(letter == '\0')
       {
            terminado=1;
            break;
       }
       int c=0;
       if(total == num){
           terminado=1;
           int k=0;
           if(x>0){x+=1;}
           for(;k<=25;k+=1){
                letter =  (char)(buffer[x]);
                x+=1;
                if(letter=='\0' || letter=='\n'|| letter==','){
                    text[c]='\0';
                    break;
                }else{
                    text[c]=letter;
                    c+=1;
                }
           }
       }
       x+=1;
    }
    return text;
}


int main(int argc, char const *argv[])
{

     key_t Clave;
    int Id_Semaforo;
    struct sembuf esperar;
    struct sembuf liberar;
    union semun arg;

    Clave = ftok ("datta.txt", 86);
    if (Clave == (key_t)-1)
    {
        printf("No puedo conseguir clave de semáforo");
        exit(0);
    }

    Id_Semaforo = semget (Clave, 10, 0600 | IPC_CREAT);
    if (Id_Semaforo == -1)
    {
        printf("No puedo crear semáforo");
        exit (0);
    }

    esperar.sem_num = 0;
    esperar.sem_op = -1;
    esperar.sem_flg = 0;

    liberar.sem_num = 0;
    liberar.sem_op = 1;
    liberar.sem_flg = 0;

     semop (Id_Semaforo, &esperar, 1);


	int total=quantityLines();
	printf("Content-type: text/html\n\n");
    printf("<html>\n");
    printf("<head><title>Get_Telemetry</title></head>\n");
    printf("<body><h2>Get_Telemetry</h2><pre>");

    if(total == 0)
    {
        printf("No data today");
    }else{

        char *last = lastLine();
        printf("%s\n", last);

        printf("Tiempo: %s \n", getForm(last,0) );
        printf("Precipitación: %s \n", getForm(last,1) );
        printf("Humedad Relativa: %s \n", getForm(last,2) );
        printf("Temperatura 1.5m: %s \n", getForm(last,3) );
        printf("Temperatura a 10cm: %s \n", getForm(last,4) );
        printf("\n");
    }

        semop (Id_Semaforo, &liberar, 1);
	return 0;
}